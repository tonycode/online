
/**
 * 路由表
 */

import Index from './components/index/index';

export default function ( router ) {
	
	router.map({
		'/': {
			component: Index
		},
		'/details': {
			component: function ( resolve ) {
				require(['./components/details/index'], resolve)
			}
		},
		//秒杀订单（收货地址空页面）
		'/seckill-shipping-address-empty': {
			component: function ( resolve ) {
				require(['./components/seckill-shipping-address-empty/index'], resolve)
			}
		},
		//秒杀订单（支付成功提示页面）
		'/seckill-pay-success': {
			component: function ( resolve ) {
				require(['./components/seckill-pay-success/index'], resolve)
			}
		},
		//秒杀订单（收货地址列表）
		'/seckill-shipping-address-list': {
			component: function ( resolve ) {
				require(['./components/seckill-shipping-address-list/index'], resolve)
			}
		},
		//门店评论
		'/store-detail': {
			component: function ( resolve ) {
				require(['./components/store-detail/store-reviews'], resolve)
			}
		},
		'/seckill-edit': {
			component: function ( resolve ) {
				require(['./components/seckill-edit/index'], resolve)
			}
		},
		//门店列表
		'/storelist': {
			component: function ( resolve ) {
				require(['./components/store-list/store-list'], resolve)
			}
		},
		//门店详情
		'/storedetail': {
			component: function ( resolve ) {
				require(['./components/store-detail/index'], resolve)
			}
		},
		//秒杀确认订单
		'/seckill-confirm-order':{
			component:function(resolve){
				require(['./components/seckill-confirm-order/index'],resolve)
			}
		},
		'/choose-coupon': {
			component: function ( resolve ) {
				require(['./components/choose-coupon/index'], resolve)
			}
		},
		//门店确认订单
		'/store-confirm-order':{
	            component:function(resolve){
	            		require(['./components/store-confirm-order/index'],resolve)
	            }
		},
		// 秒杀详情
		'/seckill-details':{
	            component:function(resolve){
	            		require(['./components/seckill-details/index'],resolve)
	            }
		},
		// 套餐卡-服务选择
		'/card-service-choose':{
	            component:function(resolve){
	            		require(['./components/card-service-choose/index'],resolve)
	            }
		},
		// 套餐卡-订单提交
		'/card-confirm-order':{
	            component:function(resolve){
	            		require(['./components/card-confirm-order/index'],resolve)
	            }
		},
		// 套餐卡-支付成功
		'/card-pay-success':{
	            component:function(resolve){
	            		require(['./components/card-pay-success/index'],resolve)
	            }
		},


		// 套餐卡-支付成功
		'/crad-list':{
	            component:function(resolve){
	            		require(['./components/crad-list/index'],resolve)
	            }
		},

		// 套餐卡-支付成功
		'/crad-list':{
	            component:function(resolve){
	            		require(['./components/crad-list/index'],resolve)
	            }
		},
		
		// 套餐卡- 卡详情
		'/card-details':{
	            component:function(resolve){
	            		require(['./components/card-details/index'],resolve)
	            }
		},
		// 套餐卡- 添加套餐卡弹出框
		'/card-add':{
	            component:function(resolve){
	            		require(['./components/card-add/index'],resolve)
	            }
		},
		// 套餐卡-购买套餐卡
		'/card-buy':{
	            component:function(resolve){
	            		require(['./components/card-buy/index'],resolve)
	            }
		},


		'*': {
			component: function ( resolve ) {
				require(['./components/notFile'], resolve)
			}
		}
	});
	
}

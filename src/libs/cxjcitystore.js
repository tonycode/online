/**
 * array扩展工具remove包含判断
 */
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;
    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;
    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

Array.prototype.remove = function(b) {
    var a = this.indexOf(b);
    if (a >= 0) {
        this.splice(a, 1);
        return true;
    }
    return false;
};

/**
 * array扩展工具contains包含判断
 */
Array.prototype.contains = function(obj) {
	var i = this.length;
	while (i--) {
		if (this[i] === obj) {
			return true;
		}
	}
	return false;
}
function remainArray (arrAll,arrRemove) {
	  var temp = []; //临时数组1

    var temparray = []; //临时数组2

    for (var i = 0; i < arrRemove.length; i++) {
        temp[arrRemove[i]] = true; //巧妙地方：把数组B的值当成临时数组1的键并赋值为真
    }

    for (var i = 0; i < arrAll.length; i++) {
        if (!temp[arrAll[i]]) {
            temparray.push(arrAll[i]); //巧妙地方：同时把数组A的值当成临时数组1的键并判断是否为真，如果不为真说明没重复，就合并到一个新数组里，这样就可以得到一个全新并无重复的数组
        };

    }
	return temparray

}
/**
 * 城市下面所有的区域-排序函数
 */
function sortDistNameNumber(a,b){
	return a.distId - b.distId;
}

/**
 * 初始化scopeInfo
 * 用于页面服务范围默认顺序
 */
function initScopeInfo(){
	var scopeInfo = new Array;
	var scopeObject = new Object();
	scopeObject.code="";
	scopeObject.name="洗车";
	scopeInfo.push(scopeObject);
	
	scopeObject = new Object();
	scopeObject.code="";
	scopeObject.name="保养";
	scopeInfo.push(scopeObject);
	
	scopeObject = new Object();
	scopeObject.code="";
	scopeObject.name="美容";
	scopeInfo.push(scopeObject);
	
	scopeObject = new Object();
	scopeObject.code="";
	scopeObject.name="易损件";
	scopeInfo.push(scopeObject);
	
	scopeObject = new Object();
	scopeObject.code="";
	scopeObject.name="轮胎";
	scopeInfo.push(scopeObject);
	
	scopeObject = new Object();
	scopeObject.code="";
	scopeObject.name="钣喷";
	scopeInfo.push(scopeObject);
	
	return scopeInfo;
}


/**
 * 获得城市下面所有的区域信息
 * @param 门店Json对象storeObject(来源于storeList的Json字符串转意)
 * @param 当前城市名称（非必填）
 * 示例：
 * currCity填入“上海”，最终返回结果第一行为“上海全部”
 * currCity不传，最终返回结果第一行为“全部”
 * @rentuen 当前所有门店总的服务范围标签Array
 */
function getPageAllDistName(storeObject,currCity){
	var result = new Array;
	if(typeof storeObject == "undefined" || !(typeof storeObject === 'object' && !isNaN(storeObject.length))){
		return result;
	}
	var distObject = null;
	var i=0,j=0;
	var flag = false;
	
	distObject = new Object;
	distObject.distId = "";
	if(typeof currCity == "undefined"){
		currCity = "";
	}
	distObject.distName = currCity + "全部";
	result.push(distObject);
	
	for(i=0;i<storeObject.length;i++){
		flag = false;
		if(typeof storeObject[i].distName != "undefined"){
			for(j=0;j<result.length;j++){
				if(result[j].distName == storeObject[i].distName){
					flag = true;
					break;
				}
			}
			if(!flag){
				distObject = new Object;
				distObject.distId = storeObject[i].distId;
				distObject.distName = storeObject[i].distName;
				result.push(distObject);
			}
		}
	}
	result.sort(sortDistNameNumber);
	return result;
}

/**
 * 获得城市下面所有的区域信息
 * @param 门店Json对象storeObject(来源于storeList的Json字符串转意)
 * 
 * @rentuen 当前所有门店总的服务范围标签Array
 */
function getPageAllScope(storeObject){
	var pageResult = new Array;
	if(typeof storeObject == "undefined" || !(typeof storeObject === 'object' && !isNaN(storeObject.length))){
		return result;
	}
	var scopeObject = null;
	var i=0,j=0,index;
	var defaultScope = initScopeInfo();
	var scopeMap;
	for(i=0;i<storeObject.length;i++){
		if(typeof storeObject[i].serviceScopeMap != "undefined"){
			scopeMap = storeObject[i].serviceScopeMap;
			for(index in scopeMap){
				for(j=0;j<defaultScope.length;j++){
					if(scopeMap[index] == defaultScope[j].name){
						defaultScope[j].code = index;
						break;
					}
				}
	        }
		}
	}
	for(j=0;j<defaultScope.length;j++){
		if(defaultScope[j].code != ""){
			pageResult.push(defaultScope[j]);
		}
	}
	return pageResult;
}

/**
 * 根据传入的查询条件返回需要隐藏的门店ID
 * @param 当前页面中选中的区编码集合selectedDistIdArray(来源于getPageAllDistName方法中对象属性distId)
 * @param 当前页面中选中的服务范围编码集合selectedScopeCodeArray(来源于getPageAllScope方法中对象属性code)
 * @param 门店Json对象storeObject(来源于storeList的Json字符串转意)
 */
function getHidenStoreId(selectedDistIdArray,selectedScopeCodeArray,storeObject){
	var storeIdArray = new Array;
	if(typeof storeObject == "undefined" || !(typeof storeObject === 'object' && !isNaN(storeObject.length))){
		return storeIdArray;
	}
	var storeShowArray=[];
	var storeAllArray=[];
	var i=0,j=0,index;
	/*加载所有门店id*/
	for(i=0;i<storeObject.length;i++){
		if(typeof storeObject[i].storeId != "undefined"){
			storeIdArray.push(storeObject[i].storeId);
			storeAllArray.push(storeObject[i].storeId);
		}
	}
	storeShowArray=storeIdArray;
	/*根据selectedDistIdArray删除非区域内的门店id*/
	if(typeof selectedDistIdArray != "undefined" 
		&& (typeof selectedDistIdArray === 'object' 
			&& !isNaN(selectedDistIdArray.length)
			&& selectedDistIdArray.length > 0)){
		for(i=0;i<storeObject.length;i++){
			if(typeof storeObject[i].distId != "undefined"){
				//相同区下的门店不隐藏
				if(selectedDistIdArray.contains(storeObject[i].distId)
					&& typeof storeObject[i].storeId != "undefined"){
					storeIdArray.remove(storeObject[i].storeId);
				}
			}
		}
	}
	if(selectedDistIdArray.length == 0){
		storeIdArray=[];
		remain=storeAllArray;
	}
	//得到显示的门店数组	
//	for (var k=0;k<storeIdArray.length;k++) {
//		if (storeShowArray.contains(storeIdArray[k])) {
//			storeShowArray.remove(storeIdArray[k]);
//		}
//	}
	var remain=remainArray(storeAllArray,storeIdArray);
	/*根据selectedScopeCodeArray删除非服务范围的门店id*/
	if(typeof selectedScopeCodeArray != "undefined" 
		&& (typeof selectedScopeCodeArray === 'object' 
			&& !isNaN(selectedScopeCodeArray.length)
			&& selectedScopeCodeArray.length > 0)){
		var scopeMap;
		var flag = 0;
		for(i=0;i<storeObject.length;i++){
			flag = 0;
			if(typeof storeObject[i].serviceScopeMap != "undefined"){
				scopeMap = storeObject[i].serviceScopeMap;
				for(index in scopeMap){
					if(selectedScopeCodeArray.contains(index)){
						flag = flag + 1;
					}
		        }
			}
			if(flag != selectedScopeCodeArray.length
					&& typeof storeObject[i].storeId != "undefined"){
				remain.remove(storeObject[i].storeId);
			}
		}
	}
	if(selectedDistIdArray.length == 0 && selectedScopeCodeArray.length == 0){
		return storeAllArray;
	}
	return remain;
}
export { 
	remainArray,
	sortDistNameNumber,
	initScopeInfo,
	getPageAllDistName,
	getPageAllScope,
	getHidenStoreId
}


/**
 * 过滤器
 */


export default function ( Vue ) {
	
	// 去除文字中的字母
	Vue.filter('removeLetters', function ( val ) {
		return val.replace(/([\w]+)/g, function ( $1 ) {
			return '';
		});
	});
	
}


/**
 * 入口文件
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import Router from './router';
import filter from './filter';

import App from './App'

Vue.use(VueRouter);


// 自定义过滤器
filter(Vue);

var router = new VueRouter({
	// history: true,
	// hashbang: fsalse
});

Router(router);

// 开启路由
router.start(App, '#app');